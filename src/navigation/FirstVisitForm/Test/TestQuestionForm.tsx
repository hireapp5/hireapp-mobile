import React from 'react';
import {
  Card,
  Button, ListItem, CheckBox,
} from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

type Props = {
    id: string,
    name: string,
    label: string,
    variants: Array<{id: string, label: string}>
}

const TestQuestionForm:React.FunctionComponent<Props> = ({ question }) => {
  const navigation = useNavigation();
  const nextQuestion = `Question-${question.index + 1}`
    console.log(nextQuestion)
  return (
    <Card>
      <Card.Title>{question.label}</Card.Title>
      <Card.Divider />
      {question.variants.map((variant, idx) => (
        <ListItem
          key={idx}
          bottomDivider
        >
          <CheckBox
            checked
          />
          <ListItem.Content>
            <ListItem.Title>{variant.label}</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      ))}
      <Card.Divider />
      <Button
        title="Дальше"
        onPress={() => { navigation.navigate(nextQuestion); }}
      />
    </Card>
  );
};

export default TestQuestionForm;
