import React from 'react';
import TestQuestionForm from './TestQuestionForm';
import { Stack } from '../../../../App';

const fakeQuestions = [
  {
    id: 1, name: 'Question', label: 'Что такое замыкание?', variants: [{ id: 1, label: 'Язык программирования' }, { id: 2, label: 'Видимость переменной ' }],
  },
  {
    id: 2, name: 'Question', label: 'Что такое Prototype?', variants: [{ id: 1, label: 'Прототип' }, { id: 2, label: 'Не знаю ' }],
  },
  {
    id: 3, name: 'Question', label: 'Что такое __proto__?', variants: [{ id: 1, label: 'Getter Setter' }, { id: 2, label: 'Видимость переменной ' }],
  },
];

const Testing:React.FunctionComponent = () => (
  <Stack.Navigator>
    {fakeQuestions.map((question, index) => <Stack.Screen key={index} name={`Question-${index}`} children={() => <TestQuestionForm question={{ ...question, index }} />} />)}
  </Stack.Navigator>
);

export default Testing;
