import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LanguagesList from './LanguagesList';
import CourseReview from './CourseReview';
import LanguageLevel from './LanguageLevel';
import { Stack } from '../../../App';

export const FirstScreeningStack = createNativeStackNavigator();

const FirstScreening:React.FunctionComponent = () => (
  <Stack.Navigator>

    <Stack.Screen name="Languages" component={LanguagesList} />
    <Stack.Screen name="CourseReview" component={CourseReview} />
    <Stack.Screen name="LanguageLevel" component={LanguageLevel} />

  </Stack.Navigator>
);

export default FirstScreening;
