import React from 'react';
import {
  Card,
  Button,
} from 'react-native-elements';
import { Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const CourseReview:React.FunctionComponent = () => {
  const navigation = useNavigation();

  return (
    <Card>
      <Card.Title>Об этом курсе</Card.Title>
      <Card.Divider />
      <Text>
        {/* eslint-disable-next-line max-len */}
        Вы пройдете скрининг по вопросам, которые чаще всего спрашивают на собеседовании Middle Frontend developer
      </Text>
      <Card.Divider />
      <Text>
        JS, React Redux, Алгоритмы и структуры данных
      </Text>
      <Card.Divider />
      <Button
        onPress={() => { // @ts-ignore
          navigation.navigate('TestModule');
        }}
        title="Пройти скрининг"
      />
    </Card>
  );
};

export default CourseReview;
