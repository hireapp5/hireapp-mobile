import React from 'react';
import {
  Card, ListItem, Icon,
} from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

const LanguageLevel:React.FunctionComponent = () => {
  const navigation = useNavigation();
  return (
    <Card>
      <Card.Title>Выберите уровень</Card.Title>
      <Card.Divider />
      <ListItem onPress={() => { // @ts-ignore
        navigation.navigate({ name: 'CourseReview' });
      }}
      >
        <Icon
          type="material-community"
          name="emoticon-neutral-outline"
        />
        <ListItem.Content>
          <ListItem.Title>Junior</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem onPress={() => { // @ts-ignore
        navigation.navigate({ name: 'CourseReview' });
      }}
      >
        <Icon
          type="material-community"
          name="emoticon-devil-outline"
        />
        <ListItem.Content>
          <ListItem.Title>Middle</ListItem.Title>
        </ListItem.Content>
      </ListItem>
      <ListItem onPress={() => { // @ts-ignore
        navigation.navigate({ name: 'CourseReview' });
      }}
      >
        <Icon
          type="material-community"
          name="emoticon-devil"
        />
        <ListItem.Content>
          <ListItem.Title>Senior</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    </Card>
  );
};

export default LanguageLevel;
