import React from 'react';
import { View, StyleSheet } from 'react-native';
import { ListItem, Icon, Text } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

const languages:Array<{name: string, id: string}> = [{ name: 'Frontend developer', id: 'JS' }, { name: 'Backend developer (Java)', id: 'JAVA' }];

const LanguagesList: React.FunctionComponent = () => {
  const navigation = useNavigation();
  return (
    <View>
      <Text style={styles.text}>По какому направлению вы бы хотели пройти собеседование?</Text>
      <View>
        { languages.map((l) => {
          const getIcon = (key: string) => {
            if (key === 'JS') {
              return 'language-javascript';
            }
            if (key === 'JAVA') {
              return 'language-java';
            }
            return null;
          };
          return (
            <ListItem
              onPress={() => { // @ts-ignore
                navigation.navigate({ name: 'LanguageLevel' });
              }}
              key={l.id}
              bottomDivider
            >
              <Icon
                type="material-community"
                name={getIcon(l.id)}
              />
              <ListItem.Content>
                <ListItem.Title>{l.name}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          );
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    padding: 10,
  },
});

export default LanguagesList;
