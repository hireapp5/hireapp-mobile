import React from 'react';
// Libs
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
// Screens
import Home from './screens/Home';
import Profile from './screens/Profile';

const Tab = createMaterialBottomTabNavigator();

const HOME_NAME = 'Главная';
const ACCOUNT = 'Профиль';

const Main: React.FunctionComponent = () => (
  <NavigationContainer>
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color }) => {
          let iconName;
          const rn = route.name;

          if (rn === HOME_NAME) {
            iconName = focused ? 'home' : 'home-outline';
          }

          if (rn === ACCOUNT) {
            iconName = focused ? 'body' : 'body-outline';
          }

          return <Ionicons name={iconName} color={color} />;
        },
      })}
      initialRouteName={HOME_NAME}
    >
      <Tab.Screen name={HOME_NAME} component={Home} />
      <Tab.Screen name={ACCOUNT} component={Profile} />
    </Tab.Navigator>
  </NavigationContainer>
);

export default Main;
