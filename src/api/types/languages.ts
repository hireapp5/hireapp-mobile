type Languages = {
  code: string,
  name: string,
}

export { Languages };