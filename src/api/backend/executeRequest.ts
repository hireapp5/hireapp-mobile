// Пока нет сборщика
const globalApiEndpoint = 'http://94.139.252.103:8080/api/v1/'

export const executeRequest = async (endpointId: string) => {
  try {
    const endpoint = `${globalApiEndpoint}${endpointId}`;
    const response = await fetch(endpoint);
    const json = await response.json();
    return json;
  }
  catch (e) {
    console.error(e);
  }
}