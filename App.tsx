import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import FirstScreening from './src/navigation/FirstVisitForm/FirstScreening';
import Testing from './src/navigation/FirstVisitForm/Test/Testing';

export const Stack = createNativeStackNavigator();

export default function App() {
  return (

    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Root" component={FirstScreening} options={{ headerShown: false }} />
        <Stack.Screen name="TestModule" component={Testing} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
